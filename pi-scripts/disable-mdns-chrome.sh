#!/bin/bash
#
# This assumes you are using "Chrome" from the official Google site via the .deb or .rpm
# This may or may not work with your distributions "Chromium" or "Chrome" package!
#
# Run this script then restart Chrome, you will see it is no longer bound to UDP:5353
#
mkdir -p /etc/chromium-browser/policies/{managed,recommended} || echo FAIL, ARE YOU ROOT
chmod go-w /etc/chromium-browser/policies/ || echo FAIL, ARE YOU ROOT
cat > /etc/chromium-browser/policies/managed/managed_policy.json << 'EOF'
{ "EnableMediaRouter": false }
EOF

#
# UPDATE: Unfortunately, there is another option that must be set as of latest versions of Chrome
# stable. It can not be set in the policy file but must be set manually in chrome://flags. Without
# this additional change, Chrome will still spam IGMP group requests on all interfaces at boot-time
#
# The flag is: #enable-webrtc-hide-local-ips-with-mdns
# It must be set to 'Disabled'
#
