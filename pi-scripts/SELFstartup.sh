#!/bin/bash

hostname=ballroomc
#options for the above ballroomA ballroomB ballroomC ballroomD ballroomWest command ballroomJR classroom bigdisplynearlobby bigdisplyside bigdisplyrear

#This will set the hostname of the device
hostnamectl set-hostname $hostname

#http://10.1.0.10/index.php?room=ballroomc
sed -i 's,http://10.1.0.10/.*,http://10.1.0.10/index.php?room='$hostname',' /etc/xdg/autostart/test.desktop
# Disable any form of screen saver / screen blanking / power management
#xset s off
#xset s noblank
#xset -dpms
