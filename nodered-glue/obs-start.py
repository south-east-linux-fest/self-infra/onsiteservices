#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import time

import logging
logging.basicConfig(level=logging.INFO)

sys.path.append('../')
from obswebsocket import obsws, requests



port = 4444
password = "password"


def start():

	if(sys.argv[1] == "ballroomA"):
		host = "10.1.0.98"
	elif (sys.argv[1] == "ballroomC"):
		host = "10.1.0.111"
	elif (sys.argv[1] == "ballroomD"):
		host = "10.1.0.94"
	elif (sys.argv[1] == "ballroomWEST"):
		host = "10.1.0.88"
	else:
		print "host not found in arg1"

	ws = obsws(host, port, password)
	ws.connect()
	ws.call(requests.StartRecording())
	ws.disconnect()



start()
