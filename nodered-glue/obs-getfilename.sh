#!/bin/bash
if [ -z $1 ]
then
  host="*** Unknown host ***"
elif [ -n $1 ]
then
# otherwise make first arg as a host
  host=$1
fi

# use case statement to make decision for rental
case $host in
   ballroomA ) 
    ssh root@10.1.0.7 'lsof -F n +D /data/SELF/incoming  | grep ^n/ | cut -c2- | sort -u'
    ;;
   ballroomC)
     echo "For $rental rental is Rs.10 per k/m."
     ;;
   ballroomD)
     echo "For $rental rental is Rs.5 per k/m."
    ;;
   ballroomWEST)
      echo "For $rental rental 20 paisa per k/m."
    ;;
   *)
     echo "nohostmatched"
    ;;
esac


#ssh systemsboy@rhost.systemsboy.edu 'ls -l'
#lsof -F n +D /data/SELF/incoming  | grep ^n/ | cut -c2- | sort -u
